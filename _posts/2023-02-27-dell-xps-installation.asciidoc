---
title: "Dell XPS installation"
categories: hardware linux
excerpt: How-To to setup development tools on XPS
---

== Installation

NOTE: Before installing, it is necessary to modify some UEFI Settings. They can be accessed with F2 when booting.

* Change the SATA Mode from the default "RAID" to "AHCI".
This will allow Linux to detect the NVMe SSD.
* Disable Secure Boot.

Other useful settings:

* Turn *off* Intel TurboBoost - that usually the main cause of thermal throttling.
* Turn *on* "Intel Speed Step" - downscale CPU clock and voltage when idling
* Turn *off* "Intel Speed Shift Technology" - does not play nicely when "Intel Speed Step" is enabled.

=== Partitions

* EFI System partition(fat32) `/boot/efi` - 512MiB
* Root partition(ext4) `/` ~100GiB
* Rest is `/home` - XFS

=== OEM Kernel

In order for audio devices to work properly, latest oem kernel must be installed:
[source, bash]
----
sudo apt install linux-oem-22.04c
----

Also download and install https://packages.debian.org/sid/all/firmware-sof-signed/download[latest sof firmware, window="_blank"].

=== Kernel settings

[source, cfg]
----
GRUB_CMDLINE_LINUX_DEFAULT="mitigations=off nvidia-drm.modeset=1 mem_sleep_default=deep"
----

* `mitigations=off` - disable all optional CPU mitigations.  This
improves system performance, but it may also
expose users to several CPU vulnerabilities.
* `nvidia-drm.modeset=1` - enable the `DRIVER_MODESET` capability flag in the nvidia-drm devices so that DRM clients can use the various modesetting APIs. In addition to allowing clients that talk to the low-level DRM interface to work, it’s also necessary for some PRIME-related interoperability features.
* `mem_sleep_default=deep` - by default, the very inefficient `s2idle` suspend variant is selected, which consumes a large amount of power.

=== Kernel modules

The nouveau module is known to cause kernel panics and freezes.

One way to mitigate this would be by adding `nomodeset` to the kernel options. However it is better to completely disable it with the blacklist method (recommended):

[source, cfg]
.`/etc/modprobe.d/blacklist-dell-xps.conf`
----
blacklist nouveau
blacklist rivafb
blacklist nvidiafb
blacklist rivatv
blacklist nv
----

=== Terminal font

* install Terminus font:
[source, bash]
----
sudo apt install xfonts-terminus
----

* configure font:
[source, bash]
----
sudo dpkg-reconfigure console-setup
----

* configure GRUB(`/etc/default/grub`):
[source]
----
GRUB_TERMINAL=gfxterm
GRUB_GFXMODE=1280x1024
GRUB_GFXPAYLOAD_LINUX=keep
----

[source, bash]
----
sudo update-grub
----

== Software

=== PipeWire

[NOTE]
====
https://ubuntuhandbook.org/index.php/2022/04/pipewire-replace-pulseaudio-ubuntu-2204/[How to Use PipeWire to replace PulseAudio in Ubuntu 22.04, window="_blank"]
====

PipeWire is pre-installed out-of-the-box, and runs as background service automatically. You may check it out by running command below in terminal:

[source, bash]
----
systemctl --user status pipewire pipewire-session-manager
----

It’s not in use by default for audio output. To get started, run command to install client libraries:

[source, bash]
----
sudo apt install pipewire-audio-client-libraries libspa-0.2-bluetooth libspa-0.2-jack

# There’s a `-` in the end of the command that marks package for removal
sudo apt install wireplumber pipewire-media-session-
----

For ALSA clients to be configured to output via PipeWire, run command to copy the configure file:
[source, bash]
----
sudo cp /usr/share/doc/pipewire/examples/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d/
----

For JACK client, run command:
[source, bash]
----
sudo cp /usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-*.conf /etc/ld.so.conf.d/
sudo ldconfig
----

For Bluetooth, just remove the pulseaudio-module-bluetooth package via command:
[source, bash]
----
sudo apt remove pulseaudio-module-bluetooth
----

And, finally enable the media session by running command:
[source, bash]
----
systemctl --user --now enable wireplumber.service
----

To verify, *reboot* and run
[source, bash, highlight=2]
----
pactl info | grep "Server Name"
Server Name: PulseAudio (on PipeWire 0.3.48)
----

==== Disable HSP/HFP profile

[NOTE]
====
https://wiki.archlinux.org/title/bluetooth_headset#Disable_PipeWire_HSP/HFP_profile[Disable PipeWire HSP/HFP profile, window="_blank"]
====

[source, bash]
----
mkdir -p ~/.config/wireplumber/bluetooth.lua.d/
cp /usr/share/wireplumber/bluetooth.lua.d/50-bluez-config.lua ~/.config/wireplumber/bluetooth.lua.d/
----

[source,cfg,highlight="4,6,14,16"]
----
...
bluez_monitor.properties = {
  ...
  ["bluez5.headset-roles"] = "[ ]",
  ...
  ["bluez5.hfphsp-backend"] = "none",
}
...
bluez_monitor.rules = {
  {
    ...
    apply_properties = {
      ...
      ["bluez5.auto-connect"] = "[ a2dp_sink ]",
      ...
       ["bluez5.hw-volume"] = "[ a2dp_sink ]",
    },
    ...
  },
}
----


=== Homebrew

[TIP]
====
More info about Homebrew on linux could be found https://docs.brew.sh/Homebrew-on-Linux[here, window="_blank"].
====

[source, bash]
.Prerequisites
----
sudo apt-get install build-essential procps curl file git
----

Then follow installation instructions on https://brew.sh/[https://brew.sh/, window="_blank"]

=== ZSH

[source,bash]
----
sudo apt install zsh

# Make zsh default shell
chsh -s $(which zsh)
# Logout
----

==== Oh My Zsh

https://ohmyz.sh/#install[https://ohmyz.sh/#install, window="_blank"]

==== Use `most` as a pager for `man`

[source,bash]
----
sudo apt install most
----

[source,cfg]
.Add to `~/.zshrc`
----
export MANPAGER="/usr/bin/most -s"
----

==== Install Gruvbox color theme

* https://github.com/sbugzu/gruvbox-zsh

[source,bash]
----
curl -L https://raw.githubusercontent.com/sbugzu/gruvbox-zsh/master/gruvbox.zsh-theme > ~/.oh-my-zsh/custom/themes/gruvbox.zsh-theme
----

* Set in `~/.zshrc`:
[source,cfg]
----
ZSH_THEME="gruvbox"
SOLARIZED_THEME="dark"
----

* Download and install Nerd Font: https://nerdfonts.com/
* Copy link:{site.baseurl}/assets/2023/02/28/gruvbox.colorscheme[Gruvbox color scheme, window="_blank"] into `~/.local/share/konsole/gruvbox.colorscheme`
* Edit current profile in Konsole->Appearance. Select `Gruvbox` color scheme and `3270Medium Nerd Font Mono`

==== ZSH settings

[source, zsh]
.`~/.zshrc`
----

export MANPAGER="/usr/bin/most -s"

# https://github.com/homeport/dyff
export KUBECTL_EXTERNAL_DIFF="dyff between --omit-header --set-exit-code --truecolor on"

function git-main-diff () {
    CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    echo "\e[0;31mDouble check that ${CURRENT_BRANCH} is rebased on main!\e[0m"
    COMMITS=`git log --oneline ${CURRENT_BRANCH} ^main | wc -l`
    echo "${CURRENT_BRANCH} diff with main: ${COMMITS}"
}

function git-master-diff () {
    CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    echo "\e[0;31mDouble check that ${CURRENT_BRANCH} is rebased on master!\e[0m"
    COMMITS=`git log --oneline ${CURRENT_BRANCH} ^master | wc -l`
    echo "${CURRENT_BRANCH} diff with master: ${COMMITS}"
}


function git-squash () {
    git reset --soft HEAD~$1 && git commit
}

HIST_STAMPS="yyyy-mm-dd"

plugins=(git brew ssh-agent poetry yarn kubectl kubectx)

alias flush-dns="sudo systemd-resolve --flush-caches"
alias gw="./gradlew"
alias gw-updates="./gradlew dependencyUpdates --refresh-dependencies -Drevision=release"
alias java-flags="java -XX:+UnlockDiagnosticVMOptions -XX:+PrintFlagsFinal -version"

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"

# https://github.com/ohmyzsh/ohmyzsh/issues/11353
alias mc="SHELL=/bin/bash TERM=xterm-256color /usr/bin/mc"
----

=== Midnight Commander

[source,bash]
----
sudo apt install mc
----

[source,cfg]
.Edit `~/.config/mc/ini`:
----
skin=modarin256-defbg-thin
----

[source,cfg]
.Edit `/root/.config/mc/ini`:
----
skin=modarin256root-defbg-thin
----


=== SSH key

[source,bash]
----
ssh-keygen -t ed25519 -C "your_email@example.com"
----

=== Git

[source,bash]
----
sudo add-apt-repository ppa:git-core/ppa
sudo apt install git git-lfs git-man

git config --global user.name "Your Name"
git config --global user.email "youremail@yourdomain.com"

git config --global core.autocrlf input

# You can use an existing SSH key to sign commits and tags
git config --global gpg.format ssh
git config --global user.signingkey /home/USERNAME/.ssh/id_ed25519
# To sign all commits by default
git config --global commit.gpgsign true
----

=== Insync

https://www.insynchq.com/downloads/linux#apt[https://www.insynchq.com/downloads/linux#apt, window="_blank"]

=== KeePassXC

[source,bash]
----
sudo add-apt-repository ppa:phoerious/keepassxc
sudo apt update
sudo apt install keepassxc
----

=== Java

* https://bell-sw.com/pages/repositories/#apt-repository-deb-based-linux-distributions[Add BellSoft repository, window="_blank"]

[source,bash]
----
sudo apt-get update
sudo apt-get install bellsoft-java19
----

* Install https://www.jenv.be/[jEnv, window="_blank"]
* Install https://www.jetbrains.com/toolbox-app/[JetBrains Toolbox App, window="_blank"]

=== Podman

[source,bash]
----
sudo apt install podman docker-compose
----

[source,bash]
.For latest version:
----
sudo mkdir -p /usr/share/keyrings
curl -fsSL https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_$(lsb_release -rs)/Release.key \
  | gpg --dearmor \
  | sudo tee /usr/share/keyrings/devel_kubic_libcontainers_unstable.gpg > /dev/null
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/devel_kubic_libcontainers_unstable.gpg]\
    https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_$(lsb_release -rs)/ /" \
  | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:unstable.list > /dev/null
sudo apt-get update -qq
sudo apt-get -qq -y install podman
----

[source, bash]
----
systemctl --user enable podman.socket --now
----

[source, zsh]
.`~/.zshrc`
----
export DOCKER_HOST="unix://$(podman info -f "{{.Host.RemoteSocket.Path}}")"
----
