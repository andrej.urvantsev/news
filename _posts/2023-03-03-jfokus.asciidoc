---
title: "JFokus 2023"
categories: life java
excerpt: Thoughts about JFokus 2023
---

The biggest disappointment so far. Could be my poor choice of talks,
but very small percentage of them was of a good quality. Even less had something new in the content.

== Opening

Opening was focused on situation in Ukraine. It was a good idea for a call to donate money,
but kind of bad implementation - too long, too wordy and almost never was mentioned again after that.

More motivating and shorter speach with a couple of reminding banners here and there would work much better imho.

== Staying Human While Coding

video::-770NR0NRaU[youtube]

Non-technical. Too long and why there was that not-so-subtle appeal to go back to offices?.. What for?

== Demystifying ‘Event’ Related Software Concepts and Methodologies

video::hl1PGT0Bsp4[youtube]

An okayish introduction to event-driven concept.

== Data Oriented Programming with the JDK 19

video::YhPsy7NjpKA[youtube]

Interesting, but would be nice to show more real-life examples when and why those things can be useful.

== Take back control of your data with Event Driven Systems

video::xXPXoU29H48[youtube]

This one was actually not bad at all. Hands-on examples from a real-life.

== Growing Development Efficiency

video::C2_aPB7sFYE[youtube]

Can be summarized as "be pragmatic".

== From Zero to Hero in Kubernetes Native Java

video::FzrOxasVgG8[youtube]

That one was a scam. Instead of talking about running java on k8s that was a sales pitch for Quarkus.

Dear Red Hat stop trying to shove Quarkus in our throats.

== How to write fast Java code

video::SovDuQefCys[youtube]

Ok, CPU cache is important, but then... Minute 29 - you literally cached some values, so you don't re-read them every time in the loop. And you "Don't know what really happened here"?!! Oh, my...

== Cloud-Native Architecture

video::xjavct1DK0I[youtube]

Nate is good. It's always a pleasure to listen to his talks. See Nate in the schedule? Go there!

== Game of Loom: implementation patterns and performance implications

video::YRn10DR1sDM[youtube]

Those kind of talks why I like JFokus. We need more like this!

== Performance Testing Java Applications

video::P47q4-cPmr4[youtube]

Maybe not so much about performance testing rather than analyzing your GC pauses.

== A Practical Guide to Containerizing Your Spring Boot Application

video::2xylqlyvXyw[youtube]

How one can talk so much about so little?! That one was a complete waste of time...

== Choice is great, but not having to choose is better

video::X8VN39Dq9_8[youtube]

That's a weird one... Like, "Just follow the flow. Use whatever is already used."

== Decision Dials

video::zS9eXAtX8FU[youtube]

Venkat is another great speaker. He can talk about anything and you'll listen.
