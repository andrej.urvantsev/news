---
title: "Kubuntu XPS installation"
categories: hardware linux
excerpt: How-To to setup development tools on XPS
---

== Installation

NOTE: Before installing, it is necessary to modify some UEFI Settings. They can be accessed with F2 when booting.

* Change the SATA Mode from the default "RAID" to "AHCI".
This will allow Linux to detect the NVMe SSD.
* Disable Secure Boot.

Other useful settings:

* Turn *off* Intel TurboBoost - that usually the main cause of thermal throttling.
* Turn *on* "Intel Speed Step" - downscale CPU clock and voltage when idling
* Turn *off* "Intel Speed Shift Technology" - does not play nicely when "Intel Speed Step" is enabled.

=== Partitions

* EFI System partition(fat32) `/boot/efi` - 512MiB
* Root partition(xfs) `/` ~100GiB
* Rest is `/home` - xfs and swap partition(at least double amount of installed memory)

==== Fix ext4 settings

https://www.kubuntuforums.net/forum/currently-supported-releases/kubuntu-24-04-nitpick-noble-lts/post-installation-az/678534-essential-and-strongly-recommended-things-to-do-directly-after-a-kubuntu-24-04-lts-installation[Source: Essential and strongly recommended things to do directly after a Kubuntu 24.04 LTS installation, window="_blank"]

Make sure that you don't use `discard` option for ext4 on SSD - use `defaults` instead, because a systemd timer for `fstrim` is already running.

To not do both is recommended by e.g. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/deduplicating_and_compressing_logical_volumes_on_rhel/trim-options-on-an-lvm-vdo-volume_deduplicating-and-compressing-logical-volumes-on-rhel[Red Hat, window="_blank"] and the https://wiki.archlinux.org/title/Solid_state_drive#TRIM[ArchWiki, window="_blank"] (and openSUSE, Debian or Ubuntu's main distribution also do not do it)!

Furthermore https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/deduplicating_and_compressing_logical_volumes_on_rhel/trim-options-on-an-lvm-vdo-volume_deduplicating-and-compressing-logical-volumes-on-rhel[Red Hat, window="_blank"] and https://documentation.suse.com/de-de/sles/15-SP5/html/SLES-all/cha-filesystems.html#sec-filesystems-trouble-trim[SUSE, window="_blank"](openSUSE) also recommend in general to use a `fstrim.timer` instead of the `discard` option (and https://fedoraproject.org/wiki/Changes/EnableFSTrimTimer[Fedora, window="_blank"], Debian or Ubuntu's main distribution also use `fstrim.timer` instead) - except for special cases.

If you are unlucky, using the `discard` option in /etc/fstab https://documentation.suse.com/de-de/sles/15-SP5/html/SLES-all/cha-filesystems.html#sec-filesystems-trouble-trim[may decrease the SSD's lifetime and can lead to performance degradation, window="_blank"]. Also `fstrim` can behave unpredictably when the "discard" option is additionally set…

[source, bash]
----
sudo cp /etc/fstab /etc/fstab.orig
sudo sed -i '/ext4/ s/discard/defaults/' /etc/fstab
----

==== Mount with "noatime", "nodiratime"

Mounting your partitions with the options `noatime` and `nodiratime` will stop timestamp writes when you read files and folders.
These timestamp writes are not generally required unless you use a local mail server client such as `mutt`.

For example:

[source, cfg]
----
UUID=49E1-D264                            /boot/efi      vfat    defaults   0 2
UUID=bc47366e-1ae0-463d-8f54-e9681cc975bf /              xfs     defaults,noatime,nodiratime   0 1
UUID=afba86b8-14ab-43d0-835e-b258fabf2e8b /home          xfs     defaults,noatime,nodiratime   0 2
UUID=f9ded650-ae18-4816-a9eb-e296a1423c3d swap           swap    defaults   0 0
tmpfs                                     /tmp           tmpfs   defaults,noatime,mode=1777 0 0

----

=== Zabbly Kernel

https://github.com/zabbly/linux#installation

=== Kernel settings

[source, cfg]
----
GRUB_CMDLINE_LINUX_DEFAULT="mitigations=off nvidia-drm.modeset=1 mem_sleep_default=deep scsi_mod.use_blk_mq=1"
----

* `mitigations=off` - disable all optional CPU mitigations.  This
improves system performance, but it may also
expose users to several CPU vulnerabilities.
* `nvidia-drm.modeset=1` - enable the `DRIVER_MODESET` capability flag in the nvidia-drm devices so that DRM clients can use the various modesetting APIs. In addition to allowing clients that talk to the low-level DRM interface to work, it’s also necessary for some PRIME-related interoperability features.
* `mem_sleep_default=deep` - by default, the very inefficient `s2idle` suspend variant is selected, which consumes a large amount of power.

Last setting `scsi_mod.use_blk_mq=1` is setting different I/O scheduler.

https://wiki.ubuntu.com/Kernel/Reference/IOSchedulers[I/O, window="_blank"] schedulers in Linux are responsible for managing read and write data operations that go in and out of the SSD.
The default scheduler on most Linux distributions is the https://www.kernel.org/doc/Documentation/block/cfq-iosched.txt[Completely Fair Queuing (CFQ), window="_blank"] scheduler, which isn’t suitable for SSD.

The most suitable schedulers for the SSD are the https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/performance_tuning_guide/ch06s04s02[deadline, window="_blank"] and https://www.cloudbees.com/blog/linux-io-scheduler-tuning[noop, window="_blank"] schedulers.
However, various I/O schedulers such as `none`, `bfq`, and `mq-deadline` show minimal capacity differences for fast multi-queue SSD or NVME devices.
In these cases, it could be preferable to use `none` as the I/O scheduler to minimize CPU overhead.
This option maximizes system performance by reducing pointless processing, thereby utilizing the SSD’s built-in speed.

=== Reduce swappiness for desktop installations

As it's suggested by https://help.ubuntu.com/community/SwapFaq#What_is_swappiness_and_how_do_I_change_it.3F[Ubuntu, window="_blank"].

[source, bash]
----
echo -e "# Reduce swappiness for desktop installation (default = 60)\nvm.swappiness=10" | sudo tee /etc/sysctl.d/99-sysswappiness.conf
----

=== Reduce systemd timeouts for desktop installations

So, the system will not "hang" for 90 seconds and longer from time to time when logging out, rebooting or shutting down.

KDE suggests for Plasma in their https://community.kde.org/Distributions/Packaging_Recommendations#Systemd_configuration[Distributions/Packaging Recommendations, window="_blank"].

[source, bash]
----
sudo mkdir -p /etc/systemd/system.conf.d && echo -e "# Reduce timeout (default = 90s)\n\n[Manager]\nDefaultTimeoutStopSec=15s" | sudo tee /etc/systemd/system.conf.d/99-systemtimeout.conf
sudo mkdir -p /etc/systemd/user.conf.d && echo -e "# Reduce timeout (default = 90s)\n\n[Manager]\nDefaultTimeoutStopSec=15s" | sudo tee /etc/systemd/user.conf.d/99-usertimeout.conf
----

=== Terminal font

* install Terminus font:
[source, bash]
----
sudo apt install xfonts-terminus
----

* configure font:
[source, bash]
----
sudo dpkg-reconfigure console-setup
----

* configure GRUB(`/etc/default/grub`):
[source]
----
GRUB_TERMINAL=gfxterm
GRUB_GFXMODE=1280x1024
GRUB_GFXPAYLOAD_LINUX=keep
----

[source, bash]
----
sudo update-grub
----

== Software

=== PipeWire: Disable HSP/HFP profile

[NOTE]
====
https://wiki.archlinux.org/title/bluetooth_headset#Disable_PipeWire_HSP/HFP_profile[Disable PipeWire HSP/HFP profile, window="_blank"]
====

[source, bash]
----
mkdir -p ~/.config/wireplumber/bluetooth.lua.d/
cp /usr/share/wireplumber/bluetooth.lua.d/50-bluez-config.lua ~/.config/wireplumber/bluetooth.lua.d/
----

[source,cfg,highlight="4,6,14,16"]
----
...
bluez_monitor.properties = {
  ...
  ["bluez5.headset-roles"] = "[ ]",
  ...
  ["bluez5.hfphsp-backend"] = "none",
}
...
bluez_monitor.rules = {
  {
    ...
    apply_properties = {
      ...
      ["bluez5.auto-connect"] = "[ a2dp_sink ]",
      ...
       ["bluez5.hw-volume"] = "[ a2dp_sink ]",
    },
    ...
  },
}
----


=== Homebrew

[TIP]
====
More info about Homebrew on linux could be found https://docs.brew.sh/Homebrew-on-Linux[here, window="_blank"].
====

[source, bash]
.Prerequisites
----
sudo apt-get install build-essential procps curl file git
----

Then follow installation instructions on https://brew.sh/[https://brew.sh/, window="_blank"]

=== ZSH

[source,bash]
----
sudo apt install zsh

# Make zsh default shell
chsh -s $(which zsh)
# Logout
----

==== Oh My Zsh

https://ohmyz.sh/#install[https://ohmyz.sh/#install, window="_blank"]

==== Use `most` as a pager for `man`

[source,bash]
----
sudo apt install most
----

[source,cfg]
.Add to `~/.zshrc`
----
export MANPAGER="/usr/bin/most -s"
----

==== Install Gruvbox color theme

* https://github.com/sbugzu/gruvbox-zsh

[source,bash]
----
curl -L https://raw.githubusercontent.com/sbugzu/gruvbox-zsh/master/gruvbox.zsh-theme > ~/.oh-my-zsh/custom/themes/gruvbox.zsh-theme
----

* Set in `~/.zshrc`:
[source,cfg]
----
ZSH_THEME="gruvbox"
SOLARIZED_THEME="dark"
----

* Download and install Nerd Font: https://nerdfonts.com/
* Copy link:{site.baseurl}/assets/2023/02/28/gruvbox.colorscheme[Gruvbox color scheme, window="_blank"] into `~/.local/share/konsole/gruvbox.colorscheme`
* Edit current profile in Konsole->Appearance. Select `Gruvbox` color scheme and `3270Medium Nerd Font Mono`

==== ZSH settings

[source, zsh]
.`~/.zshrc`
----

export MANPAGER="/usr/bin/most -s"

# https://github.com/homeport/dyff
export KUBECTL_EXTERNAL_DIFF="dyff between --omit-header --set-exit-code --truecolor on"

function git-main-diff () {
    CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    echo "\e[0;31mDouble check that ${CURRENT_BRANCH} is rebased on main!\e[0m"
    COMMITS=`git log --oneline ${CURRENT_BRANCH} ^main | wc -l`
    echo "${CURRENT_BRANCH} diff with main: ${COMMITS}"
}

function git-master-diff () {
    CURRENT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    echo "\e[0;31mDouble check that ${CURRENT_BRANCH} is rebased on master!\e[0m"
    COMMITS=`git log --oneline ${CURRENT_BRANCH} ^master | wc -l`
    echo "${CURRENT_BRANCH} diff with master: ${COMMITS}"
}


function git-squash () {
    git reset --soft HEAD~$1 && git commit
}

HIST_STAMPS="yyyy-mm-dd"

plugins=(git brew ssh-agent poetry yarn kubectl kubectx)

alias gw="./gradlew"
alias gw-updates="./gradlew dependencyUpdates --refresh-dependencies -Drevision=release"
alias java-flags="java -XX:+UnlockDiagnosticVMOptions -XX:+PrintFlagsFinal -version"

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
----

=== Midnight Commander

[source,bash]
----
sudo apt install mc
----

[source,cfg]
.Edit `~/.config/mc/ini`:
----
skin=modarin256-defbg-thin
----

[source,cfg]
.Edit `/root/.config/mc/ini`:
----
skin=modarin256root-defbg-thin
----


=== SSH key

[source,bash]
----
ssh-keygen -t ed25519 -C "your_email@example.com"
----

=== Git

[source,bash]
----
sudo add-apt-repository ppa:git-core/ppa
sudo apt install git git-lfs git-man

git config --global user.name "Your Name"
git config --global user.email "youremail@yourdomain.com"

git config --global core.autocrlf input

# You can use an existing SSH key to sign commits and tags
git config --global gpg.format ssh
git config --global user.signingkey /home/USERNAME/.ssh/id_ed25519
# To sign all commits by default
git config --global commit.gpgsign true
----

=== Insync

https://www.insynchq.com/downloads/linux#apt[https://www.insynchq.com/downloads/linux#apt, window="_blank"]

=== KeePassXC

[source,bash]
----
sudo add-apt-repository ppa:phoerious/keepassxc
sudo apt update
sudo apt install keepassxc
----

=== Java

* Install https://sdkman.io/install[SDKMAN!, window="_blank"]
* Install https://www.jetbrains.com/toolbox-app/[JetBrains Toolbox App, window="_blank"]

=== Podman

* Follow https://podman.io/docs/installation[Podman Installation Instructions, window="_blank"]

[source, bash]
----
systemctl --user enable podman.socket --now
----

[source, zsh]
.`~/.zshrc`
----
export DOCKER_HOST="unix://$(podman info -f "{{.Host.RemoteSocket.Path}}")"
----
